# Python introduction

## Object-oriented programming is a good idea.
> Object-oriented programming (OOP) is a programming paradigm based on the concept of "objects", which can contain data, in the form of fields (often known as attributes or properties), and code, in the form of functions (often known as methods). [^1]


<br>

For us, a great analogy for an object is an <i>element</i>. It's a general and abstract concept.
<br>

## Python is a dynamically and strongly typed programming language.
## What is a type?
<hr \>


### A type is a way of taking the abstract "object" and adding features to it.
<b>There are many pre-defined "types" in python for you to take advantage of.</b>
<br>
Some times, however, those types will not suffice. When those times arise, we can turn to defining our own types. In general, that looks like: [^2]

```python
class ClassName:
    <statement-1>
    .
    .
    .
    <statement-N>
```  

<br>
Where the ClassName becomes the type.

<br>

When we go over types and essential programming in greater detail, I'll give you the full treatment on typing. For now, you already know some essentials:
1. Boolean 
```python 
bool
```
2. Integer 
```python 
int
```

## Instantiating new values:
```python
#Some integers:
x = 1
y = -1
#Some booleans
a = False
b = True

#Only fancy thing "unpacking" assignments:
t, greatest = 23, True
```

[^1] [https://en.wikipedia.org/wiki/Object-oriented_programming]
[^2] [https://docs.python.org/3/tutorial/classes.html]